/**
 * @file std_functions.c
 * @author Richard Oare
 * @date 15 Jan 2019
 * This file provides basic funcitonality to be used in other files.
 * @copyright GNU Public License
 * @page License
 * Copyright (C) 2019 Richard Oare
 * All Rights Reserved.
 *
 * daq-record is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "std_functions.h"

/** Variable to for testing endianness. */
const int test_endianness = 1;

/** Compares two strings to check if they are a match ignoring case
 @param[in] s1 First string
 @param[in] s2 Second string
 @return 0 if they match, -1 if one or both strings are NULL, or the
 difference betewen s1 and s2
 */
int my_strcmp(char *s1, char *s2) {
    unsigned int i;
    
    if (s1 == NULL || s2 == NULL) return -1;
    for (i = 0; s1[i] != '\0' && s2[i] != '\0'; i++) {
        if (toupper(s1[i]) != toupper(s2[i])) return s1[i] - s2[i];
    }
    if (s1[i] == s2[i]) return 0;
    else return -2; //Shouldn't be able to hit this case
}

/** Decodes user input from text to cmd_val and parses arguments
 @param[in] line User input string
 @param[out] args Parsed Arguments
 @param[in] maxNumArgs Maximum number of allowed arguments
 @return cmd_val (non-zero value) or 0 if invalid
 */
enum cmd_val decode_cmd(char *line, char **args, int maxNumArgs) {
    char *cmd = strtok(line, WHITESPACE);
    enum cmd_val cmdv;
    int i;
    
    for (i = 0; 0 != my_strcmp(cmd, (char*) COMMAND_LIST[i]) && NULL != COMMAND_LIST[i]; i++);
    
    if (NULL == COMMAND_LIST[i]) return 0;
    else {
        cmdv = i+1;
        args[0] = NULL;
        for (i = 0; i < maxNumArgs && NULL != (cmd = strtok(NULL, WHITESPACE)); i++) args[i] = cmd;
        if (i < maxNumArgs && args[i] != NULL) args[i] = NULL;
    }
        
    return cmdv;
}

/** Ensures correct range of arguments are input
 @param[in] args User input arguments
 @param[in] minNumArgs Minimum number of allowed arguments
 @param[in] maxNumArgs Maximum number of allowed arguments
 @param[in] v Whether or not to be verbose
 @return true or false on whether or not the number of arguments fall
 into the specified range
 */
bool correct_num_args(char **args, int minNumArgs, int maxNumArgs, bool v) {
    int i;
    
    //Case: No Arguments Allowed
    if (args[0] == NULL && maxNumArgs == 0) return true;
    else if (maxNumArgs == 0) {
        if (v) fprintf(stderr, "%s", "No Arguments Allowed\n");
        return false;
    }
    
    //Ensure Minimum Number of Arguments
    for (i = 0; i < minNumArgs; i++) {
        if (args[i] == NULL) {
            if (v) fprintf(stderr, "%s", "Not Enough Arguments\n");
            return false;
        }
    }
    
    //Ensure Maximum Number of Arguments
    while (args[i] != NULL && i < maxNumArgs) i++;
    if (args[i] != NULL) {
        if (v) fprintf(stderr, "%s", "Too Many Args\n");
        return false;
    }
    
    return true;
}
