/**
 * @file control.c
 * @author Richard Oare
 * @date 31 Jan 2019
 * This file provides basic control functions for inputting sample sets
 * and basic DAQ functionality.  Some flags are set here to change the output
 * and program flow.
 * @copyright GNU Public License
 * @page License
 * Copyright (C) 2019 Richard Oare
 * All Rights Reserved.
 *
 * daq-record is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "control.h"

/** Infile line pointer. */
samp *daqEnd;
/** Infile line pointer. */
char daqBuffer[MAX_RESP_LEN];
/** Infile line pointer. */
static unsigned int lowest_chan;
/** DAQ location character pointer. */
char *daqLoc = NULL;
/** Flag for user input.  This is input during program execution. */
static bool userin = false;
/** Flag for exit on completion. */
static bool exitoncompletion = false;
/** Flag for verbose. */
static bool verbose = false;

/** Sets the value of userin
 @param[in] v Value to set userin to
 @return None
 */
void set_userin(bool v) {
    userin = v;
}

/** Returns the value of userin
 @return userin value
 */
bool get_userin() {
    return userin;
}

/** Sets the value of exitoncompletion
 @param[in] v Value to set exitoncompletion to.
 @return None
 */
void set_exit(bool v) {
    exitoncompletion = v;
}

/** Returns the value of exitoncompletion
 @return exitoncompletion value
 */
bool get_exit() {
    return exitoncompletion;
}

/*********************INFILE FUNCTIONS*********************/
/** initializes the queue based on input from the list of
 infile sample sets
 @return true if all samples found were input correctly,
 false otherwise.
 */
//TODO: account for being unable to malloc
bool init_infile_sample_set() {
    bool rtn = true;
    char *tempSS[5];
    unsigned int n;
    ssize_t len;
    
    while (0 < (len = read_line()) && rtn) {
        tempSS[(n = 0)] = strtok(infile_line, TOK_CHARS);
        while (NULL != tempSS[n] && n < 4) tempSS[++n] = strtok(NULL,TOK_CHARS);
        rtn = (n == 4 && tempSS[n] == NULL) ? (NO_ERROR == push_sample_set(strtoul(tempSS[0],NULL,10), strtof(tempSS[1],NULL), atoi(tempSS[3]), strtof(tempSS[2],NULL))) : false;
    }
    close_infile();
    return rtn;
}

/*********************DAQ FUNCTIONS*********************/
/** Resets the DAQ by writing a reset code to the DAQ
 @return None
 */
void reset_daq() {
    strcpy(daqBuffer, RESET_COMMAND);
    write_DAQ(daqBuffer);
    close_DAQ(false);
    sleep(1);
    open_DAQ();
}

/** Sends the CRPT command to the DAQ to check if there is
 corrupt samples on the DAQ or not.  Prints the output.
 @return True if corrupt, false otherwise
 */
bool daq_crpt() {
    int i;
    
    strcpy(daqBuffer, CRPT_COMMAND);
    write_DAQ(daqBuffer);
    if ((i = read_DAQ(MAX_RESP_LEN)) > 0) {
        daq_read[i] = '\0';
        printf("%s\n",daq_read);
        if (!strcmp(daq_read,"TRUE")) return true;
        else return false;
    }
}

/** Sends the SAMPRDY command to the DAQ to check how
 many samples are ready.  Prints the output.
 @return None
 */
void daq_samprdy() {
    int i;
    
    strcpy(daqBuffer, SAMPRDY_COMMAND);
    write_DAQ(daqBuffer);
    if ((i = read_DAQ(MAX_RESP_LEN)) > 0) {
        daq_read[i] = '\0';
        printf("%s\n",daq_read);
    }
}

/** Sends the QRY command to the DAQ to check the values
 of a sample set at a specific spot in the queue.  Prints
 the output.
 @param[in] s Position in queue.
 @return True if nothing exists, false otherwise
 */
bool daq_qry_ss(unsigned int s) {
    char ss[MAX_LINE_SIZE - 10];
    int i;
    
    strcpy(daqBuffer, QRY_COMMAND);
    snprintf(ss,(MAX_LINE_SIZE - 10)," %u",s);
    strcat(daqBuffer,ss);
    write_DAQ(daqBuffer);
    if ((i = (int) read_DAQ(MAX_RESP_LEN)) > 0) {
        daq_read[i] = '\0';
        printf("%s\n",daq_read);
        if (!strcmp(daq_read, "Does Not Exist")) return true;
        else return false;
    }
}

/** Sends the RREG command to the DAQ to check the value
 of a specific register.  Prints the output.
 @param[in] r Register number on the DAQ.
 @return None
 */
void daq_read_reg(unsigned int r) {
    char rr[MAX_LINE_SIZE - 10];
    int i;
    
    if (r > 23) return;
    
    strcpy(daqBuffer, RREG_COMMAND);
    snprintf(rr,(MAX_LINE_SIZE - 10)," %u",r);
    strcat(daqBuffer,rr);
    write_DAQ(daqBuffer);
    if ((i = read_DAQ(MAX_RESP_LEN)) > 0) {
        daq_read[i] = '\0';
        printf("%s\n",daq_read);
    }
}

/** Pops a sample from both the DAQ queue and the queue
 herein.
 @return None
 */
void rm_sample() {
    int i;
    
    rm_begin();
    strcpy(daqBuffer, RM_COMMAND);
    write_DAQ(daqBuffer);
    if ((i = read_DAQ(MAX_RESP_LEN)) > 0) {
        daq_read[i] = '\0';
        printf("%s\n",daq_read);
    }
}

/** Pushes a sample set to both the DAQ queue and the
 queue herein.  Sample is pushed to the queue here first
 with push_sample_set.  If successful, the sample set is
 then pushed to the DAQ queue using send_ss_to_daq.  If
 that fails, both sample sets are removed and an error
 returned.
 @param[in] numSamples Number of samples for sample set
 @param[in] rate Sampling rate for sample set
 @param[in] channels Channel bitmask for sample set
 @param[in] mult Multiplier for sample set
 @return CE_NO_ERROR if successful, CE_BAD_RESPONSE if
 uncessful on host, or CE_NULL_SS if uncessful on DAQ
 (This case should not be hit).
 */
enum controlErrorTypes add_sample_set(unsigned long numSamples, float rate, unsigned int channels, float mult) {
    if (NO_ERROR != push_sample_set(numSamples,rate,channels,mult)) return CE_BAD_RESPONSE;
    else if (NO_ERROR != send_ss_to_daq(find_end())) {
        rm_end(); //Remove SS that was pushed into host queue
        return CE_NULL_SS;
    }
    return CE_NO_ERROR;
}

/** Writes the sample list from the host to the DAQ.
 @return Sample pointer of next element to be pushed to
 the DAQ.  NULL if it pushed all elements.
 */
samp* write_list_to_DAQ() {
    samp *temp = queue_ptr();
    
    if (temp == NULL) return NULL;
    else while (0 == send_ss_to_daq(temp)) temp = temp->next;
    
    return temp;
}

/** Sends a single sample set to the DAQ.
 @return CE_NO_ERROR if successful, CE_BAD_RESPONSE if
 uncessful, or CE_NULL_SS if sample set input is NULL.
 */
enum controlErrorTypes send_ss_to_daq(samp* ss) {
    unsigned char n = 0;
    int i;
    
    if (ss == NULL) return CE_NULL_SS;
    
    snprintf(daqBuffer,MAX_RESP_LEN,"%s %lu %f %u",ADD_COMMAND,ss->num,ss->rate,ss->channels);
    write_DAQ(daqBuffer);
    
    if ((i = read_DAQ(MAX_RESP_LEN)) > 0) daq_read[i] = '\0';
    
    if (strcmp(ADD_SUCCESS_RESPONSE,daq_read) == 0) return CE_NO_ERROR;
    else return CE_BAD_RESPONSE;
}

/** Starts or stops the DAQ
 @param[in] s True to start, False to stop
 @return None
 */
void start_daq(bool s) {
    int i;
    
    if (NULL == queue_ptr()) return;
    
    (s) ? strcpy(daqBuffer,START_COMMAND) : strcpy(daqBuffer,STOP_COMMAND);
    write_DAQ(daqBuffer);
    if ((i = read_DAQ(MAX_RESP_LEN)) > 0) daq_read[i] = '\0';
    
    if (!s) {
        read_DAQ_values();
        stop_timer();
        wr_ss("stop");
    }
    else {
        wr_ss("start");
        interrupt_timer(&read_DAQ_values,MIN(DELAY_SAMPLES,num_samples_left()),cur_rate());
    }
}

/** Converts data read from the DAQ to a floating point
 value.
 @param[in] data Byte array to convert
 @return Converted data as floating point
 */
float data_to_value(char *data) {
    return (float) ((((data[0] & 0x80) ? 0xff : 0) << 24) | (data[0] << 16) | (data[1] << 8) | (data[2]))*(float) (ADC_CONVERSION);
}

/** Reads data from the DAQ.  Data is converted and correlated
 with the correct channel number.  The data is written if the
 channel is to be written else it is ignored.  This is continued
 until all data in the buffer is exhausted or the last sample set
 has been processed.
 @return None
 */
//TODO: Implement Timeout Here
void read_DAQ_values() {
    static unsigned char chan = 0;
    static unsigned int i = 0;
    float *ptr = daq_read_val;
    ssize_t valsRead = read_DAQ_vals(DAQ_READ_SIZE)/sizeof(float);
    static unsigned int vtRead = 0;
    enum ssInfo info = SS_SAME;
    
    /*fflush(stdout);
    printf("\nREAD: %d\t TOTAL: %d\n",valsRead,vtRead += valsRead);
    fflush(stdout);*/

    if (valsRead == 0) valsRead = read_DAQ_vals(DAQ_READ_SIZE)/sizeof(float);
    if (valsRead == 1) {
        if (daq_crpt()) quit_prog(EXIT_FAILURE);
    }
    else if (valsRead > 0) {
        //printf("\nRead: %d\n TOTAL Read: %d\n",valsRead,(i+= valsRead));
        
        /*valsRead = valsRead/sizeof(float);
        for (i = 0; i < valsRead; i++) {
            printf("%f\t",daq_read_val[i]);
        }
        printf("END\n\n");*/
        
        do {
            if (get_channels() & (1 << chan)) {
                write_value_to_outfile((*ptr)*cur_mult(), chan, cur_rate(), (SS_SAME != (info = dec_value())));
                if (info == SS_NEW) {
                    chan = 0;
                    interrupt_timer(&read_DAQ_values,MIN(DELAY_SAMPLES, 6 * num_samples_left()),cur_rate());
                }
            }
            if (info != SS_NEW && info != SS_DONE && info != SS_NONE) {
                if (++chan > 5) chan = 0;
            }
        } while ((float*) (++ptr) < (float*) &(daq_read_val[valsRead]));
    }
    
    
    if (num_samples_left() < DELAY_SAMPLES) interrupt_timer(&read_DAQ_values,num_samples_left(),cur_rate());
    
    if (info == SS_DONE || info == SS_NONE) {
        if (exitoncompletion) quit_prog(EXIT_SUCCESS);
        else {
            chan = 0;
            stop_timer();
        }
    }
}

/** Quits the program safely with exit code given by
 status.
 @param[in] status Exit code to use to exit program
 @return None
 */
void quit_prog(int status) {
    close_infile();
    close_DAQ(true);
    close_outfile();
    exit(status);
}
