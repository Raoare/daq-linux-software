/**
 * @file daqIO.h
 * @author Richard Oare
 * @date 31 Jan 2019
 * This file provides definitions for daqIO.c
 * @copyright GNU Public License
 * @page License
 * Copyright (C) 2019 Richard Oare
 * All Rights Reserved.
 *
 * daq-record is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef DAQIO_H
#define DAQIO_H

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/select.h>

#include "file_lock.h"

/** Channel extension, only used with format = OUTPUT_DAT. */
#define CHANNEL_EXT "-c"
/** CSV file extension. */
#define CSV_FILE_EXT ".CSV"
/** DAT file extension. */
#define DAT_FILE_EXT ".DAT"
/** Header for Time. */
#define TIME_HEADER "Time"
/** Header for Channel. */
#define CHANNEL_HEADER "CH"
/** Maximum number of outfiles. */
#define MAX_NUM_OUTFILES 6

/** Default Absolute DAQ location. */
#define DEFAULT_DAQ_LOC "/dev/usbtmc0"
/** Maximum number of bytes to read from DAQ. */
#define DAQ_READ_SIZE 20000

/** Formats of how to write to the outfile. */
typedef enum outputFormats {
    OUTPUT_STDOUT,  /*!< Value for outfile to stdout */
    OUTPUT_CSV,     /*!< Value for outfile to .CSV */
    OUTPUT_DAT      /*!< Value for outfile to .DAT, opens multiple files */
}outFmt;

/** @cond INCLUDE_WITH_DOXYGEN */
extern char *infile_line;
extern char daq_read[DAQ_READ_SIZE];
extern float daq_read_val[DAQ_READ_SIZE];

void close_all(int status);
bool is_infile_open();
bool open_infile(char *file);
void close_infile();
ssize_t read_line();
bool is_daq_open();
void set_daq_location(char *loc);
int open_DAQ();
void close_DAQ(bool end);
void write_DAQ(char *str);
ssize_t read_DAQ(unsigned int amt);
ssize_t read_DAQ_vals(unsigned int amt);
bool is_outfile_open();
void set_record_time(bool v);
bool get_record_time();
void set_outfile_format(outFmt f);
bool open_outfile(char *file, bool append);
void close_outfile();
void write_value_to_outfile(float val, unsigned int channel, float sample_rate, bool new_ss);
void wr_ss(char *ss);
/** @endcond */

#endif
