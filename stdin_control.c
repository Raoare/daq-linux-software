/**
 * @file stdin_control.c
 * @author Richard Oare
 * @date 15 Jan 2019
 * This file provides control for in-program user commands
 * @copyright GNU Public License
 * @page License
 * Copyright (C) 2019 Richard Oare
 * All Rights Reserved.
 *
 * daq-record is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "stdin_control.h"

/** Global variable to return number of samples for sample set. */
uint32_t stdin_val;
/** Global variable to return channel bitmask for sample set. */
uint32_t stdin_c;
/** Global variable to return sample rate for sample set. */
float stdin_rate;
/** Global variable to return multiplier for sample set. */
float stdin_mult;

/** Fetches and decodes user input during execution
 @param[in] v T/F to be verbose or not
 @return cmd_val
 */
enum cmd_val fetch_decode(bool v) {
    char line[MAX_LINE_SIZE];
    char *args[MAX_NUM_ARGS];
    enum cmd_val cmd = 0;
    
    while ((NULL == fgets(line, MAX_LINE_SIZE, stdin)));

    switch (cmd = decode_cmd(line, args, MAX_NUM_ARGS)) {
        case CMD_START:
            if (!correct_num_args(args,0,0,v)) cmd = 0;
            break;
        case CMD_STOP:
            if (!correct_num_args(args,0,0,v)) cmd = 0;
            break;
        case CMD_ADD:
            if (!correct_num_args(args,4,4,v)) cmd = 0;
            else {
                stdin_val = strtol(args[0], NULL, 10);
                stdin_rate = strtof(args[1], NULL);
                stdin_c = strtol(args[2], NULL, 10);
                stdin_mult = strtof(args[3], NULL);
            }
            break;
        case CMD_QRY:
            if (!correct_num_args(args,1,1,v)) cmd = 0;
            else stdin_c = strtol(args[0], NULL, 10);
            break;
        case CMD_RREG:
            if (!correct_num_args(args,1,1,v)) cmd = 0;
            else stdin_c = strtol(args[0], NULL, 10);
            break;
        case CMD_RM:
            if (!correct_num_args(args,0,0,v)) cmd = 0;
            break;
        case CMD_RST:
            if (!correct_num_args(args,0,0,v)) cmd = 0;
            break;
        case CMD_CRPT:
            if (!correct_num_args(args,0,0,v)) cmd = 0;
            break;
        case CMD_SAMPRDY:
            if (!correct_num_args(args,0,0,v)) cmd = 0;
            break;
            //TODO: this should be a different kind of help
        case CMD_HELP:
            if (!correct_num_args(args,0,0,v)) cmd = 0;
            else printf("%s", USAGE_HELP);
            break;
        default:
            if (v) fprintf(stderr, "%s", "Invalid Command\n");
            break;
    }
    
    return cmd;
}
