/**
 * @file std_functions.h
 * @author Richard Oare
 * @date 15 Jan 2019
 * This file provides defnitions for std_function.c
 * @copyright GNU Public License
 * @page License
 * Copyright (C) 2019 Richard Oare
 * All Rights Reserved.
 *
 * daq-record is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef STD_FUNCTIONS_H
#define STD_FUNCTIONS_H

#include <stddef.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <stdint.h>
#include <math.h>

/** @cond INCLUDE_WITH_DOXYGEN */
const int test_endianness;
/** @endcond */

/** Checks for bigendianness */
#define is_bigendian() ( (*(char*)&test_endianness) == 0 )

/** Maximum size of the line allowed to be input by the user */
#define MAX_LINE_SIZE 255
/** Number of arguments allowed to be input by the user */
#define MAX_NUM_ARGS 5
/** Whitespace used for separation */
#define WHITESPACE " \t\n"

/** START Command as Defined on the Seven-of-Nine DAQ. */
#define START_COMMAND "START"
/** STOP Command as Defined on the Seven-of-Nine DAQ. */
#define STOP_COMMAND "STOP"
/** ADD Command as Defined on the Seven-of-Nine DAQ. */
#define ADD_COMMAND "ADD"
/** QRY Command as Defined on the Seven-of-Nine DAQ. */
#define QRY_COMMAND "QRY"
/** RST Command as Defined on the Seven-of-Nine DAQ. */
#define RESET_COMMAND "RST"
/** RM Command as Defined on the Seven-of-Nine DAQ. */
#define RM_COMMAND "RM"
/** RREG Command as Defined on the Seven-of-Nine DAQ. */
#define RREG_COMMAND "RREG"
/** CRPT Command as Defined on the Seven-of-Nine DAQ. */
#define CRPT_COMMAND "CRPT"
/** SAMPRDY Command as Defined on the Seven-of-Nine DAQ. */
#define SAMPRDY_COMMAND "SAMPRDY"
/** HELP Command as Defined on the Seven-of-Nine DAQ. */
#define HELP_COMMAND "HELP"

/** List of Commands as Defined on the Seven-of-Nine DAQ. */
#define COMMAND_LIST ((char const*[]) {START_COMMAND, STOP_COMMAND, ADD_COMMAND, QRY_COMMAND, RREG_COMMAND, RM_COMMAND, RESET_COMMAND, CRPT_COMMAND, SAMPRDY_COMMAND, HELP_COMMAND, NULL})

/*! Used to Convert User Text to Value.*/
enum cmd_val {
    CMD_START = 1,  /*!< Command for START */
    CMD_STOP,       /*!< Command for STOP */
    CMD_ADD,        /*!< Command for ADD */
    CMD_QRY,        /*!< Command for QRY */
    CMD_RREG,       /*!< Command for RREG */
    CMD_RM,         /*!< Command for RM */
    CMD_RST,        /*!< Command for RST */
    CMD_CRPT,       /*!< Command for CRPT */
    CMD_SAMPRDY,    /*!< Command for SAMPRDY */
    CMD_HELP        /*!< Command for HELP */
};

/** ADDED Response as Defined on the Seven-of-Nine DAQ. */
#define ADD_SUCCESS_RESPONSE "ADDED"
/** FULL Response as Defined on the Seven-of-Nine DAQ. */
#define FULL_RESPONSE "FULL"
/** INVALID Response as Defined on the Seven-of-Nine DAQ. */
#define INVALID_RESPONSE "INVALID"

/** Program help for allowed arguments to main.c. */
#define PROGRAM_USAGE_HELP "Valid arguments include:\n -o {outfile} [Default: stdout]\n -i {infile} [Default: None]\n -s {Number of Samples} [Default: 2000]\n -r {Sample Rate} [Default: 2000]\n -c Channels [Default: All (63)]\n -m {Multiplication} [Default: 1]\n -d {Device} [Default: /dev/usbtmc1]\n -e {External Pipe} [Default: None]\n -f {Output Format (STDOUT, CSV, DAT)} [Default: STDOUT]\n -t Record Timestamp with Each Sample [Default: No Timestamp with Each Sample]\n -e Exit Upon Completion [Default: Continue Waiting]\n -a Append Output File [Default: Write]\n -u Execute with User Input [Default: No Input Until Initial Job Complete]\n -v Make This Program Verbose [Default: Limited Feedback]\n -h This Help Page\n"

/** Program help for allowed arguments to stdin_control.c. */
#define USAGE_HELP "Valid arguments include:\n -o {outfile} [Default: stdout]\n -i {infile} [Default: None]\n -s {Number of Samples} [Default: 2000]\n -r {Sample Rate} [Default: 2000]\n -c Channels [Default: All (63)]\n -m {Multiplication} [Default: 1]\n -d {Device} [Default: /dev/usbtmc1]\n -e {External Pipe} [Default: None]\n -f {Output Format (STDOUT, CSV, DAT)} [Default: STDOUT]\n -h This Help Page\n"

/** @cond INCLUDE_WITH_DOXYGEN */
int my_strcmp(char *s1, char *s2);
enum cmd_val decode_cmd(char *line, char **args, int maxNumArgs);
bool correct_num_args(char **args, int minNumArgs, int maxNumArgs, bool v);
/** @endcond */

#endif
