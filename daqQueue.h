/**
 * @file daqQueue.h
 * @author Richard Oare
 * @date 15 Jan 2019
 * This file provides definitions for daqQueue.c
 * @copyright GNU Public License
 * @page License
 * Copyright (C) 2019 Richard Oare
 * All Rights Reserved.
 *
 * daq-record is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef daqQueue_h
#define daqQueue_h

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

/** DAQ Sample Rate Limits as Defined on the Seven-of-Nine DAQ. */
#define DAQ_RATE_LIMITS ((const float[]) {0.001,8000.0})
/** DAQ Channel Limits as Defined on the Seven-of-Nine DAQ. */
#define DAQ_CHANNEL_LIMITS ((const unsigned int[]) {1,63})

/*! Sample Set Definition.*/
typedef struct sample {
    unsigned long num;      /*!< Number of Samples to Take */
    float rate;             /*!< Sample Rate to use */
    float mult;             /*!< Multiplication to use on Sample Set */
    unsigned char channels; /*!< Channels Bitmask */
    unsigned char numCh;    /*!< Total Number of Channels */
    struct sample *next;    /*!< Pointer to the Next Sample Set */
}samp;

/*! Numerical Definition of Queue Push Errors.*/
enum daqQueueErrorTypes {
    NO_ERROR,       /*!< No Error */
    MALLOC_ERROR,   /*!< Can't Allocate More Memory */
    INVALID_SS      /*!< Invalid Sample Set */
};

/*! Sample Set Information on Position in Queue.*/
enum ssInfo {
    SS_DONE,    /*!< Sample Sets are Done, Nothing Else in Queue */
    SS_NEW,     /*!< Previous Sample Set Finished and Another is Pulled from the Queue */
    SS_SAME,    /*!< Working on the Same Sample Set */
    SS_NONE     /*!< Queue is Empty */
};

/** @cond INCLUDE_WITH_DOXYGEN */
samp* queue_ptr();
samp* find_end();
void print_all_SS();
enum daqQueueErrorTypes push_sample_set(unsigned long numSamples, float rate, unsigned int channels, float mult);
void print_all_SS();
bool rm_end();
bool rm_n_element(unsigned int n);
bool rm_begin();
enum ssInfo dec();
enum ssInfo dec_value();
unsigned long num_samples_left();
float cur_rate();
float cur_mult();
bool queue_exists();
unsigned int get_channels();

/** @endcond */

#endif
