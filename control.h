/**
 * @file control.h
 * @author Richard Oare
 * @date 15 Jan 2019
 * This file provides definitions for control.c
 * @copyright GNU Public License
 * @page License
 * Copyright (C) 2019 Richard Oare
 * All Rights Reserved.
 *
 * daq-record is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef CONTROL_H
#define CONTROL_H

/** -- Includes -- */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

/** program specific includes */

#include "daqQueue.h"
#include "timer.h"
#include "daqIO.h"
#include "std_functions.h"

/** DAQ resolution in bytes. */
#define NUM_BYTES_RESOLUTION 3
#define NUM_CHANNELS 6
/** DAQ ADC voltage. */
#define ADC_VOLTAGE 5.2
/** Conversion factor. */
#define ADC_CONVERSION (float) ((float)ADC_VOLTAGE/((unsigned long) 0x7FFFFF))
/** Token characters to use in strtok. */
#define TOK_CHARS ", \n"
/** Maximum response length for responses from the DAQ. */
#define MAX_RESP_LEN 100
/** Maximum number of samples to delay before reading from the DAQ. */
#define DELAY_SAMPLES 300

/** Macro for calculating minimum number. */
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

/** Control error codes. */
enum controlErrorTypes {
    CE_NO_ERROR,        /*!< No Error */
    CE_BAD_RESPONSE,    /*!< Bad Response Error */
    CE_NULL_SS          /*!< Null Sample Set Error */
};

/** @cond INCLUDE_WITH_DOXYGEN */
void set_userin(bool v);
bool get_userin();
void set_exit(bool v);
bool get_exit();
bool init_infile_sample_set();
void reset_daq();
bool daq_crpt();
void daq_samprdy();
bool daq_qry_ss(unsigned int s);
void daq_read_reg(unsigned int r);
void rm_sample();
enum controlErrorTypes add_sample_set(unsigned long numSamples, float rate, unsigned int channels, float mult);
samp* write_list_to_DAQ();
enum controlErrorTypes send_ss_to_daq(samp *ss);
void start_daq(bool s);
void read_DAQ_values();
float data_to_value(char *data);
void quit_prog(int status);
/** @endcond */

#endif /* _CONTROL_H_*/
