/**
 * @file main.c
 * @author Richard Oare
 * @version 1.0
 * @date 15 Jan 2019
 * This program controls the seven-of-nine data aquisition device.
 * This relies on a Unix system, the
 * @copyright GNU Public License
 * @page License
 * Copyright (C) 2019 Richard Oare
 * All Rights Reserved.
 *
 * daq-record is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

/*! -- Includes -- */

#include <sys/time.h>
#include <sys/resource.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/** program specific includes */
#include "control.h"
#include "daqQueue.h"
#include "std_functions.h"
#include "stdin_control.h"
#include "signal_handler.h"
#include "timer.h"

/** Error comment returned when user tries to input both a input file and one time sample set. */
#define ERROR_INPUT_SS_AND_INFILE "Cannot Input Sample Sets Both Ways"
/** Error comment returned when user tries to input an input file that cannot be opened. */
#define ERROR_BAD_INFILE "Bad File Descriptor for Infile"
/** Error comment returned when the program has a bad location to the DAQ. */
#define ERROR_BAD_DAQ "Bad File Descriptor for DAQ"
/** Error comment returned when input file values cannot be parsed and sent correctly. */
#define ERROR_BAD_INFILE_VALUES "Infile Values Invalid"
/** Error comment returned when user tries to input an output file that cannot be opened. */
#define ERROR_BAD_OUTFILE "Output File Invalid"
/** Error comment returned when user tries to input an invalid ouptut file format. */
#define ERROR_BAD_OUTFILE_FORMAT "Invalid Output Format"
/** Error comment returned when user tries to input an invalid arguement. */
#define ERROR_INVALID_ARGUEMENT "Invalid Option, Use -h for Help"
/** Error comment returned when user tries to input multiple input files. */
#define ERROR_MULTIPLE_INPUT "Cannot Open Multiple Infiles"
/** Error comment returned when nothing to do. */
#define ERROR_NOTHING_TO_DO "Nothing to do, exit"
/** Valid input arguements.  Single character only. */
#define VALID_ARGUEMENTS "o:i:s:r:c:m:d:f:theauv"
/** Warning for an invalid sample set. */
#define WARN_INVALID_SS "Sample Set Invalid"

/** Prints error and exits program
 @param[in] err Error message to print to stderr
 @return None
 */
void error(char *err) {
    fprintf(stderr, "%s\n", err);
    quit_prog(EXIT_FAILURE);
}

/** Exits the program with failure
 @return None
 */
void bad_exit() {
    quit_prog(EXIT_FAILURE);
}

/** Exits the program with Success
 @return None
 */
void good_exit() {
    quit_prog(EXIT_SUCCESS);
}

/** Program starting point
 @param[in] argc Number of arguements input
 @param[in] argv Parameters
 @return code for success or failure
 */
int main(int argc, char *argv[]) {
    int c;
    bool ssInput = true, verbose = false, append = false;
    char *outfileName = NULL;
    struct sample temp_sample = {2000,2000,1,63,6,NULL};
    
    init_sig(&bad_exit,&good_exit);
    
    while ((c = getopt(argc, argv, VALID_ARGUEMENTS)) != -1) {
        switch (c) {
            case 'o': //Outfile
                outfileName = optarg;
                break;
            case 'i': //Infile
                if (queue_ptr() != NULL) error(ERROR_MULTIPLE_INPUT);
                if (open_infile(optarg) < 0) error(ERROR_BAD_INFILE);
                if (!init_infile_sample_set()) error(ERROR_BAD_INFILE_VALUES);
                ssInput = false;
                break;
            case 's': //Number of Samples
                if (!ssInput) error(ERROR_INPUT_SS_AND_INFILE);
                temp_sample.num = strtoul(optarg,NULL,10);
                break;
            case 'r': //Sample Rate
                if (!ssInput) error(ERROR_INPUT_SS_AND_INFILE);
                temp_sample.rate = strtof(optarg,NULL);
                break;
            case 'c': //Channels to Record
                if (!ssInput) error(ERROR_INPUT_SS_AND_INFILE);
                temp_sample.channels = strtoul(optarg,NULL,10);
                break;
            case 'm': //Multiplier
                if (!ssInput) error(ERROR_INPUT_SS_AND_INFILE);
                temp_sample.mult = strtof(optarg,NULL);
                break;
            case 'd': //DAQ Location
                set_daq_location(optarg);
                break;
            case 'f': //Format
                if (0 == my_strcmp(optarg,"STDOUT")) set_outfile_format(OUTPUT_STDOUT);
                else if (0 == my_strcmp(optarg,"CSV")) set_outfile_format(OUTPUT_CSV);
                else if (0 == my_strcmp(optarg,"DAT")) set_outfile_format(OUTPUT_DAT);
                else error(ERROR_BAD_OUTFILE_FORMAT);
                break;
            case 'h': //Help
                printf("%s",PROGRAM_USAGE_HELP);
                break;
            case 'u': //User Input
                set_userin(true);
                break;
            case 't': //Record Timestamp
                set_record_time(true);
                break;
            case 'v': //Verbose Output
                verbose = true;
                break;
            case 'e': //Exit Upon Completion
                set_exit(true);
                break;
            case 'a': //Append to File(s)
                append = true;
                break;
            case '?':
                error(ERROR_INVALID_ARGUEMENT);
                break;
            default:
                abort();
                break;
        }
    }
    if (queue_ptr() == NULL && !ssInput && !get_userin())
        error(ERROR_NOTHING_TO_DO);
    
    if (!get_userin() && !get_exit()) set_exit(true);
    
    // Make sure DAQ is open or open it
    if (!is_daq_open()) {
        if ((c = open_DAQ()) < 0) error(ERROR_BAD_DAQ);
        reset_daq();
    }
    
    if(!open_outfile(outfileName, append)) error(ERROR_BAD_OUTFILE);
    
    // Write sample set input
    if (queue_ptr() != NULL) {
        write_list_to_DAQ();
    }
    else if (ssInput) {
        add_sample_set(temp_sample.num, temp_sample.rate, temp_sample.channels, temp_sample.mult);
    }
    if (!get_userin()) start_daq(true);
    
    while (1) {
        if (get_userin()) {
            switch (fetch_decode(true)) {
                case CMD_START:
                    start_daq(true);
                    break;
                case CMD_STOP:
                    start_daq(false);
                    break;
                case CMD_ADD:
                    if (CE_NO_ERROR != add_sample_set(stdin_val,stdin_rate,stdin_c,stdin_mult)) fprintf(stderr, "%s\n", WARN_INVALID_SS);
                    break;
                case CMD_QRY:
                    daq_qry_ss(stdin_c);
                    break;
                case CMD_RREG:
                    daq_read_reg(stdin_c);
                    break;
                case CMD_RM:
                    rm_sample();
                    break;
                case CMD_RST:
                    reset_daq();
                    break;
                case CMD_CRPT:
                    daq_crpt();
                    break;
                case CMD_SAMPRDY:
                    daq_samprdy();
                    break;
                default:
                    break;
            }
        }
    }
}
