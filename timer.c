/**
 * @file timer.c
 * @author Richard Oare
 * @date 15 Jan 2019
 * This file provides timer functionality relying on the SIGALRM signal.
 * @copyright GNU Public License
 * @page License
 * Copyright (C) 2019 Richard Oare
 * All Rights Reserved.
 *
 * daq-record is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "timer.h"

/** Creates an interval interrupt timer.  Expiration is at
 num_samp/rate seconds
 @param[in] handler Function to call upon timer completion
 @param[in] num_samp Used to calculate timer expiration
 @param[in] rate Used to calculate timer expiration
 @return None
 */
bool interrupt_timer(void (*handler) (void), unsigned int num_samp, float rate) {
    float temp = num_samp/rate;
    struct sigaction sa;
    struct itimerval timer;
    
    memset(&sa, 0, sizeof (sa));
    sa.sa_handler = (void*) handler;
    if (0 != sigaction(SIGALRM, &sa, NULL)) return false;
    
    //printf("NS: %d\t rate: %f\t TVAL: %f\n",num_samp, rate, temp);
    timer.it_value.tv_sec = floor(temp);
    timer.it_value.tv_usec = (temp - floor(temp))*pow(10,6);
    timer.it_interval.tv_sec = timer.it_value.tv_sec;
    timer.it_interval.tv_usec = timer.it_value.tv_usec;
    
    if (0 != setitimer(ITIMER_REAL, &timer, NULL)) return false;
    return true;
}

/** Stops the interval timer.
 @return None
 */
void stop_timer() {
    struct sigaction sa;
    memset (&sa, 0, sizeof (sa));
    sa.sa_handler = SIG_IGN;
    sigaction(SIGALRM, &sa, NULL);
}
