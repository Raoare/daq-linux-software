/**
 * @file daqIO.c
 * @author Richard Oare
 * @date 14 Aug 2019
 * This file provides IO functionality for input file, output files, and
 * the DAQ itself.
 * @copyright GNU Public License
 * @page License
 * Copyright (C) 2019 Richard Oare
 * All Rights Reserved.
 *
 * daq-record is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "daqIO.h"

/** Infile line pointer. */
char *infile_line = NULL;
/** DAQ read byte array. */
char daq_read[DAQ_READ_SIZE];
float daq_read_val[DAQ_READ_SIZE];

static char *daq_loc = NULL;
static outFmt fmt = OUTPUT_STDOUT;
static int daq = 0;
fd_set set;
struct timeval timeout;
static FILE *infile = NULL, *outfile[MAX_NUM_OUTFILES] = {NULL,NULL,NULL,NULL,NULL,NULL};
static bool recTime = false;

/*********************INFILE FUNCTIONS*********************/
/** Returns if infile is open
 @return true if infile is open, false otherwise
 */
bool is_infile_open() {
    return (infile != NULL);
}

/** Opens infile for read
 @param[in] file String giving absolute path to infile
 @return true if file is open for read
 */
bool open_infile(char *file) {
    return ((0 >= infile) ? (0 < (infile = fopen(file,"r"))) : (0 < infile));
}

/** Closes the infile
 @return None
 */
void close_infile() {
    if (infile > 0) fclose(infile);
    infile = NULL;
    if (NULL != infile_line) {
        free(infile_line);
        infile_line = NULL;
    }
}

/** Reads a line from the infile
 @return Number of bytes read
 */
ssize_t read_line() {
    size_t l = 0;
    return getline(&infile_line, &l, infile);
}

/*********************DAQ FUNCTIONS*********************/
/** Returns if DAQ is open
 @return true if DAQ is open, false otherwise
 */
bool is_daq_open() {
    return (daq != 0);
}

/** Sets the location of the DAQ
 @param[in] loc String to absolute DAQ path
 @return None
 */
void set_daq_location(char *loc) {
    if (daq_loc != NULL) {
        free(daq_loc);
        daq_loc = NULL;
    }
    daq_loc = malloc(1+strlen(loc));
    strcpy(daq_loc,loc);
}

/** Opens the DAQ unless its already open
 @return daq file number
 */
int open_DAQ() {
    if (NULL == daq_loc) set_daq_location(DEFAULT_DAQ_LOC);
    if (daq == 0) {
        if ((daq = open(daq_loc,O_RDWR)) < 0) {
            perror("Error opening DAQ");
            daq = 0;
        }
        FD_ZERO(&set);
        FD_SET(daq,&set);
        timeout.tv_sec = 0;
        timeout.tv_usec = 100000;
        lock_f(daq);
    }
    return daq;
}

/** Closes the DAQ
 @return None
 */
void close_DAQ(bool end) {
    if (daq != 0) {
        ulock_f(daq);
        close(daq);
        daq = 0;
    }
    if (end && (daq_loc != NULL)) {
        free(daq_loc);
        daq_loc = NULL;
    }
}

/** Writes to the DAQ
 @return None
 */
void write_DAQ(char *str) {
    if (daq != 0) write(daq,str,strlen(str)+1);
}

/** Reads from the DAQ
 @param[in] amt Maximum number of bytes to read
 @return Number of bytes read
 */
ssize_t read_DAQ(unsigned int amt) {
    if (daq != 0) return read(daq,daq_read,amt);
    else return 0;
}

ssize_t read_DAQ_vals(unsigned int amt) {
    int rv;
    
    if (daq != 0) {
        /*rv = select(daq+1,&set,NULL,NULL,&timeout);
        if (rv == -1) perror("select");
        else if (rv == 0) printf("TIMEOUT\n");
        else return read(daq,daq_read_val,amt);*/
        return read(daq,daq_read_val,amt);
    }
    return 0;
}

/*********************OUTFILE FUNCTIONS*********************/
/** Returns if outfile is open
 @return true if outfile is open, false otherwise
 */
bool is_outfile_open() {
    int i;
    for(i = 0; i < MAX_NUM_OUTFILES; i++)
        if (outfile[i] != NULL) return true;
    return false;
}

/** Sets the outfile format
 @return None
 */
void set_outfile_format(outFmt f) {
    fmt = f;
}

/** Sets the flag to record time to the outfile
 @return None
 */
void set_record_time(bool v) {
    recTime = v;
}

/** Returns the record time flag
 @return recTime
 */
bool get_record_time() {
    return recTime;
}

/** Opens the outfile
 @param[in] file String of the absolute outfile location
 @param[in] append True if to open using append, false to
 open overwriting the file
 @return True if file is open, false otherwise
 */
bool open_outfile(char *file, bool append) {
    char temp[255], tempn[2], tempa[3];
    int i,j;
    
    if (append) strcpy(tempa, "a+");
    else strcpy(tempa, "w");
    
    if (fmt == OUTPUT_DAT) {
        for (i = 0; i < MAX_NUM_OUTFILES; i++) {
            strcpy(temp,file);
            strcat(temp,CHANNEL_EXT);
            snprintf(tempn, 2, "%d", i);
            strcat(temp,tempn);
            strcat(temp,DAT_FILE_EXT);
            if (0 > (outfile[i] = fopen(temp, tempa))) {
                outfile[i] = NULL;
                return false;
            }
        }
        return true;
    }
    else if (fmt == OUTPUT_CSV || fmt == OUTPUT_STDOUT) {
        if (fmt == OUTPUT_CSV) {
            strcpy(temp,file);
            strcat(temp,CSV_FILE_EXT);
            if (0 > (outfile[0] = fopen(temp, tempa))) {
                outfile[0] = NULL;
                return false;
            }
        }
        else outfile[0] = stdout;
        strcpy(temp, TIME_HEADER);
        for (i = 0; i < MAX_NUM_OUTFILES; i++) {
            strcat(temp, ",");
            strcat(temp, CHANNEL_HEADER);
            snprintf(tempn, 2, "%d",i);
            strcat(temp, tempn);
        }
        fprintf(outfile[0], "%s\n", temp);
        return true;
    }
    else return false;
}

/** Closes the outfiles
 @return None
 */
void close_outfile() {
    int i;
    for (i = 0; i < MAX_NUM_OUTFILES; i++) {
        if (outfile[i] != NULL && outfile[i] != stdout) fclose(outfile[i]);
        outfile[i] = NULL;
    }
}

/** Writes to the outfiles
 @param[in] val DAQ value to write
 @param[in] channel Channel being written
 @param[in] sample_rate Sample rate being used
 @param[in] new_ss True if new sample set, false otherwise
 @return None
 */
void write_value_to_outfile(float val, unsigned int channel, float sample_rate, bool new_ss) {
    static unsigned int last_channel = 0;
    static unsigned char first_write = 1;
    static float samp_time = 0;
    unsigned char i;
    
    if (channel >= MAX_NUM_OUTFILES) return;
    
    if (fmt == OUTPUT_DAT) {
        if (outfile[channel] == NULL) return;
        
        switch (first_write) {
            case 0:
                fprintf(outfile[channel],",%f",val);
                break;
            case 2:
                if (channel > last_channel) fprintf(outfile[channel],"%f",val);
                else {
                    fprintf(outfile[channel],",%f",val);
                    first_write = 0;
                }
                break;
            case 1:
                first_write = 2;
                fprintf(outfile[channel],"%f",val);
                break;
            default:
                break;
        }
        last_channel = channel;
    }
    else if (fmt == OUTPUT_CSV || fmt == OUTPUT_STDOUT) {
        if (outfile[0] == NULL) return;
        
        if (first_write == 1) {
            if (recTime) fprintf(outfile[0],"%f,",(samp_time += 1/sample_rate));
            first_write = 0;
            if (channel == 0) {
                fprintf(outfile[0],"%f",val);
                last_channel = channel;
                return;
            }
        }
        if (channel > last_channel)
            for (i = channel-last_channel; i > 0; i--) fprintf(outfile[0],",");
        else if (last_channel >= channel) {
            for (i = MAX_NUM_OUTFILES-1-last_channel; i > 0; i--) fprintf(outfile[0],",");
            if (recTime) fprintf(outfile[0],"\n%f,",(samp_time += 1/sample_rate));
            else fprintf(outfile[0],"\n");
            for (i = channel; i > 0; i--) fprintf(outfile[0],",");
        }
        fprintf(outfile[0],"%f",val);
        if (new_ss) {
            for (i = MAX_NUM_OUTFILES-1-channel; i > 0; i--) fprintf(outfile[0],",");
            last_channel = 5;
        }
        else last_channel = channel;
    }
}

void wr_ss(char *ss) {
    time_t timer;
    char buffer[26];
    struct tm* tm_info;
    
    if (fmt == OUTPUT_CSV || fmt == OUTPUT_STDOUT) {
        time(&timer);
        tm_info = localtime(&timer);
        strftime(buffer, 26, "%Y-%m-%d %H:%M:%S", tm_info);
        fprintf(outfile[0], "\n%s %s\n",ss, buffer);
    }
    
}
