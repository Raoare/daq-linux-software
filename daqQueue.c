/**
 * @file daqQueue.c
 * @author Richard Oare
 * @date 15 Jan 2019
 * This file provides functionality to control the sample set queue.
 * This includes push, pop, finding, and returning certain values.
 * @copyright GNU Public License
 * @page License
 * Copyright (C) 2019 Richard Oare
 * All Rights Reserved.
 *
 * daq-record is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "daqQueue.h"

static samp *queue = NULL;

/** Finds the end of the sample set queue
 @return Pointer to the last sample set in the queue
 */
samp* find_end() {
    samp *end = queue;
    if (end != NULL) while(end->next != NULL) end = end->next;
    return end;
}

/** Returns the queue pointer
 @return Pointer to the first sample set in the queue
 */
samp* queue_ptr() {
    return queue;
}

/** Prints all sample set information in the queue
 @return None
 */
void print_all_SS() {
    samp* temp = queue;
    while (temp != NULL) {
        printf("NUM: %lu\t RATE: %f\t MULT: %f\t CH: %d\n",temp->num, temp->rate, temp->mult, temp->channels);
        temp = temp->next;
    }
}

/** Pushes a sample set into the queue
 @param[in] numSamples Number of samples in this sample set
 @param[in] rate Sample rate for this sample set
 @param[in] channels Bitmask for channels to record
 @param[in] mult Multiplier for data from sample set
 @return Error type as defined in enum daqQueueErrorTypes
 */
enum daqQueueErrorTypes push_sample_set(unsigned long numSamples, float rate, unsigned int channels, float mult) {
    samp *temp = malloc(sizeof(samp)), *end = find_end();
    
    if (NULL == temp) return MALLOC_ERROR;
    if (rate < DAQ_RATE_LIMITS[0] || rate > DAQ_RATE_LIMITS[1] || channels < DAQ_CHANNEL_LIMITS[0] || channels > DAQ_CHANNEL_LIMITS[1]) {
        free(temp);
        return INVALID_SS;
    }
    
    //Create samp item and append to queue
    temp->num = numSamples;
    temp->rate = rate;
    temp->mult = mult;
    temp->channels = channels;
    temp->numCh = __builtin_popcount(channels);
    temp->next = NULL;
    
    if (end == NULL) queue = temp;
    else end->next = temp;
    
    return NO_ERROR;
}

/** Removes the last pointer in the queue
 @return false if queue is empty, true otherwise
 */
bool rm_end() {
    samp *end = find_end(), *prevEnd = queue;
    
    if (end == prevEnd && queue != NULL) {
        free(queue);
        queue = NULL;
    }
    else if (queue != NULL) {
        while (prevEnd->next != end) prevEnd = prevEnd->next;
        free(end);
        prevEnd->next = NULL;
    }
    return (queue != NULL);
}

/** Removes sample set n from the queue
 @param[in] n Element number to remove
 @return true if element existed, false otherwise
 */
bool rm_n_element(unsigned int n) {
    unsigned int i;
    samp *c = queue, *p = queue;
    
    if (n == 0) return rm_begin();
    else if (queue == NULL) return false;
    
    for (i = 0; i < n && c->next != NULL; i++, c = c->next) p = c;
    
    if (i != n) return false;
    else {
        p->next = c->next;
        free(c);
    }
    return true;
}

/** Pops sample set from the queue
 @return false if queue is empty, true otherwise
 */
bool rm_begin() {
    samp *temp = queue;
    
    if (queue == NULL) return false;
    queue = queue->next;
    free(temp);
    return (queue != NULL);
}

/** Decrements the number of samples remaining in current sample set.
 This indicates all channel samples were received.
 @return enum ssInfo Indicating whether a queue change was made
 */
enum ssInfo dec() {
    if (NULL == queue) return SS_NONE;
    else {
        if (0 == (--(queue->num)))
            return (rm_begin()) ? SS_NEW : SS_DONE;
    }
    return SS_SAME;
}

/** Decrements chNum to indicate a single channel sample was received
 @return enum ssInfo Indicating whether a queue change was made
 */
enum ssInfo dec_value() {
    static unsigned char chNum = 0;
    enum ssInfo rtn = SS_SAME;
    
    if (queue == NULL) rtn = SS_NONE;
    else {
        if (0 == chNum) chNum = queue->numCh;
        if (0 == --chNum) {
            if (SS_NEW == (rtn = dec())) chNum = queue->numCh;
        }
    }
    
    return rtn;
}

/** Returns number of samples left for the current sample set
 @return Number of samples left
 */
unsigned long num_samples_left() {
    return ((NULL == queue) ? 0 : queue->num);
}

/** Returns current sample rate for the current sample set
 @return Current sample rate
 */
float cur_rate() {
    return ((NULL == queue) ? 0 : queue->rate);
}

/** Returns current multiplier for the current sample set
 @return Current multiplier
 */
float cur_mult() {
    return ((NULL == queue) ? 0 : queue->mult);
}

/** Returns whether or not queue exists
 @return true if queue exists, false otherwise
 */
bool queue_exists() {
    return (NULL != queue);
}

/** Returns channel bitmask number for current sample set
 @return Channel bitmask number
 */
unsigned int get_channels() {
    return ((NULL == queue) ? 0 : queue->channels);
}
